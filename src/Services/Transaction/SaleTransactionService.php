<?php

namespace Coqmos\BrainTreeBundle\Services\Transaction;

use Braintree\Result\Error;
use Braintree\Transaction;
use Coqmos\BrainTreeBundle\DTOs\TransactionDTOInterface;
use Coqmos\BrainTreeBundle\Services\Transaction\Abstraction\BaseTransactionService;
use Coqmos\BrainTreeBundle\Exception\BrainTreeException;

class SaleTransactionService extends BaseTransactionService
{
    /**
     * @param TransactionDTOInterface $transactionDTO
     *
     * @return Transaction
     *
     * @throws BrainTreeException
     */
    public function sale(TransactionDTOInterface $transactionDTO): Transaction
    {
        $result = $this->transactionGateway
            ->sale($this->dtoToArray($transactionDTO))
        ;

        if ($result instanceof Error) {
            $this->handleError($result);
        }

        return $result->transaction;
    }

    /**
     * @param TransactionDTOInterface $transactionDTO
     *
     * @return array
     */
    private function dtoToArray(TransactionDTOInterface $transactionDTO): array
    {
        $details = [
            'amount' => $transactionDTO->getAmount(),
            'paymentMethodNonce' => $transactionDTO->getPaymentMethodNonce(),
            'options' => [
                'submitForSettlement' => true
            ]
        ];

        if (null !== $transactionDTO->getOrderId()) {
            $details['orderId'] = $transactionDTO->getOrderId();
        }

        if (null !== $transactionDTO->getDiscountAmount()) {
            $details['discountAmount'] = $transactionDTO->getDiscountAmount();
        }

        return $details;
    }
}
