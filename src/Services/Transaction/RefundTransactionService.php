<?php

namespace Coqmos\BrainTreeBundle\Services\Transaction;

use Braintree\Result\Error;
use Braintree\Result\Successful;
use Coqmos\BrainTreeBundle\DTOs\TransactionDTOInterface;
use Coqmos\BrainTreeBundle\Services\Transaction\Abstraction\BaseTransactionService;
use Coqmos\BrainTreeBundle\Exception\BrainTreeException;

class RefundTransactionService extends BaseTransactionService
{

    /**
     * @param TransactionDTOInterface $transactionDTO
     *
     * @return Successful
     *
     * @throws BrainTreeException
     */
    public function refund(TransactionDTOInterface $transactionDTO): Successful
    {
        $result = $this->transactionGateway->refund($transactionDTO->getTransactionId());

        if ($result instanceof Error) {
            return $this->handleError($result);
        }

        return $result;
    }
}
