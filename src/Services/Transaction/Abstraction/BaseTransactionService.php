<?php

namespace Coqmos\BrainTreeBundle\Services\Transaction\Abstraction;

use Braintree\TransactionGateway;
use Coqmos\BrainTreeBundle\Factory\BrainTreeFactory;
use Coqmos\BrainTreeBundle\Services\Abstraction\BrainTreeBaseService;

class BaseTransactionService extends BrainTreeBaseService
{
    /**
     * @var TransactionGateway
     */
    protected $transactionGateway;

    /**
     * @param BrainTreeFactory $brainTreeFactory
     */
    public function __construct(BrainTreeFactory $brainTreeFactory)
    {
        parent::__construct($brainTreeFactory);
        $this->transactionGateway = $this->getFactory()->transaction();
    }
}
