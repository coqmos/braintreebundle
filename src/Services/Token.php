<?php

namespace Coqmos\BrainTreeBundle\Services;

use Coqmos\BrainTreeBundle\Services\Abstraction\BrainTreeBaseService;

class Token extends BrainTreeBaseService
{
    /**
     * @return string
     */
    public function generate(): string
    {
        return $this->getFactory()
            ->clientToken()
            ->generate()
        ;
    }
}