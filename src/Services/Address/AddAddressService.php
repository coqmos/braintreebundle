<?php

namespace Coqmos\BrainTreeBundle\Services\Address;

use Braintree\Address;
use Braintree\Result\Error;
use Coqmos\BrainTreeBundle\DTOs\AddressDTOInterface;
use Coqmos\BrainTreeBundle\Exception\BrainTreeException;
use Coqmos\BrainTreeBundle\Services\Abstraction\BrainTreeBaseService;
use Braintree\Exception\NotFound;

class AddAddressService extends BrainTreeBaseService
{
    /**
     * @param AddressDTOInterface $addressDTO
     * @return Address
     *
     * @throws BrainTreeException
     * @throws NotFound
     */
    public function create(AddressDTOInterface $addressDTO): Address
    {
        $result = $this->getFactory()
            ->address()
            ->create(
                $this->dtoToArray($addressDTO)
            )
        ;

        if ($result instanceof Error) {
            $this->handleError($result);
        }

        return $result->address;
    }

    /**
     * @param AddressDTOInterface $addressDTO
     *
     * @return array
     */
    private function dtoToArray(AddressDTOInterface $addressDTO): array
    {
        $details = [];

        if (null !== $addressDTO->getCustomerId()) {
            $details['customerId'] = $addressDTO->getCustomerId();
        }

        if (null !== $addressDTO->getStreetAddress()) {
            $details['streetAddress'] = $addressDTO->getStreetAddress();
        }

        if (null !== $addressDTO->getExtendedAddress()) {
            $details['extendedAddress'] = $addressDTO->getExtendedAddress();
        }

        if (null !== $addressDTO->getLocality()) {
            $details['locality'] = $addressDTO->getLocality();
        }

        if (null !== $addressDTO->getPostalCode()) {
            $details['postalCode'] = $addressDTO->getPostalCode();
        }

        if (null !== $addressDTO->getCountryCodeAlpha2()) {
            $details['countryCodeAlpha2'] = $addressDTO->getCountryCodeAlpha2();
        }

        return $details;
    }
}