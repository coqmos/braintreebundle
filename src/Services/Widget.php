<?php

namespace Coqmos\BrainTreeBundle\Services;

use Coqmos\BrainTreeBundle\Services\Token as Token;

class Widget
{
    /**
     * @var Token
     */
    private $token;

    public function __construct(Token $token)
    {
        $this->token = $token;
    }

    /**
     * @param float $amount
     * @return string
     *
     */
    public function create(float $amount)
    {
        return '
        <html>
			<head>
            <link rel=stylesheet type=text/css href="http://movimentgraffitti.org/assets/plugins/braintree/braintree.css">
            <link rel=stylesheet type=text/css href="http://movimentgraffitti.org/assets/plugins/braintree/braintreeoverrides.css">
            <script src="http://movimentgraffitti.org/assets/js/jquery.min.js"></script>
            <script src="http://movimentgraffitti.org/assets/plugins/braintree/braintree.js"></script>

        <script src="https://js.braintreegateway.com/v2/braintree.js"></script>
            </head>
			<body>
              <div class="wrapper">
                    <div class="checkout container">
                        <form method="post" id="payment-form" action="http://localhost:8080/api/test">
                            <div id="bt-dropin"></div>
                            <div class="amount-wrapper">
                                <input type="submit" value="Pay" class="button"/>
                            </div>
                        </form>
                
                <script>
                $(document).ready(function(){
                    var client_token = "' .$this->token->generate().'";
                    braintree.setup(client_token, "dropin", {
                        container: "bt-dropin",
                        paypal: {
                            singleUse: false,
                            amount: '.$amount.',
                            currency: "EUR"
                        }
                    });
                });
                </script>
                </div>

			</body>
			</html>
        ';
    }
}