<?php

namespace Coqmos\BrainTreeBundle\Services\Customer\Abstraction;

use Braintree\CustomerGateway;
use Coqmos\BrainTreeBundle\Factory\BrainTreeFactory;
use Coqmos\BrainTreeBundle\Services\Abstraction\BrainTreeBaseService;

class BaseCustomerService extends BrainTreeBaseService
{
    /*** @var CustomerGateway */
    protected $customer;

    /**
     * {@inheritdoc}
     */
    public function __construct(BrainTreeFactory $brainTreeFactory)
    {
        parent::__construct($brainTreeFactory);
        $this->customer = $this
            ->getFactory()
            ->customer()
        ;
    }
}
