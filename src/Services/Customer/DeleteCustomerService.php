<?php

namespace Coqmos\BrainTreeBundle\Services\Customer;

use Braintree\Result\Error;
use Coqmos\BrainTreeBundle\DTOs\CustomerDTOInterface;
use Coqmos\BrainTreeBundle\Exception\BrainTreeException;
use Coqmos\BrainTreeBundle\Services\Customer\Abstraction\BaseCustomerService;
use Braintree\Result\Successful;

class DeleteCustomerService extends BaseCustomerService
{
    /**
     * @param CustomerDTOInterface $customerDTO
     *
     * @return Successful
     *
     * @throws BrainTreeException
     */
    public function delete(CustomerDTOInterface $customerDTO): Successful
    {
        $result = $this->customer->delete($customerDTO->getId());

        if ($result instanceof Error) {
            return $this->handleError($result);
        }

        return $result;
    }
}
