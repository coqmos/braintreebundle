<?php

namespace Coqmos\BrainTreeBundle\Services\Customer;

use Braintree\Customer;
use Braintree\Result\Error;
use Coqmos\BrainTreeBundle\DTOs\AddressDTOInterface;
use Coqmos\BrainTreeBundle\DTOs\CustomerDTOInterface;
use Coqmos\BrainTreeBundle\Exception\BrainTreeException;
use Coqmos\BrainTreeBundle\Factory\BrainTreeFactory;
use Coqmos\BrainTreeBundle\Services\Address\AddAddressService;
use Coqmos\BrainTreeBundle\Services\Customer\Abstraction\BaseCustomerService;
use Braintree\Exception\NotFound;

class AddCustomerService extends BaseCustomerService
{
    /** @var AddAddressService */
    private $addAddress;

    /**
     * @param BrainTreeFactory $brainTreeFactory
     * @param AddAddressService $addAddress
     */
    public function __construct(BrainTreeFactory $brainTreeFactory, AddAddressService $addAddress)
    {
        parent::__construct($brainTreeFactory);
        $this->addAddress = $addAddress;
    }

    /**
     * @param CustomerDTOInterface $customerDTO
     *
     * @return Customer
     *
     * @throws BrainTreeException
     * @throws NotFound
     */
    public function create(CustomerDTOInterface $customerDTO): Customer
    {
        $result = $this->customer
            ->create($this->dtoToArray($customerDTO))
        ;

        if ($result instanceof Error) {
            $this->handleError($result);
        }

        if (!empty($customerDTO->getAddresses())) {
            foreach ($customerDTO->getAddresses() as $address) {
                /** @var AddressDTOInterface $address */
                $address->setCustomerId($result->customer->id);
                $this->addAddress->create($address);
            }
        }

        return $result->customer;
    }

    /**
     * @param CustomerDTOInterface $customerDTO
     *
     * @return array
     */
    private function dtoToArray(CustomerDTOInterface $customerDTO): array
    {
        $details = [
            'firstName' => $customerDTO->getFirstName(),
            'email'     => $customerDTO->getEmail(),
        ];

        if (null !== $customerDTO->getLastName()) {
            $details['lastName'] = $customerDTO->getLastName();
        }

        if (null !== $customerDTO->getPhone()) {
            $details['phone'] = $customerDTO->getPhone();
        }

        if (null !== $customerDTO->getWebsite()) {
            $details['website'] = $customerDTO->getWebsite();
        }

        if (null !== $customerDTO->getCompany()) {
            $details['company'] = $customerDTO->getCompany();
        }

        if (null !== $customerDTO->getPaymentMethodNonce()) {
            $details['paymentMethodNonce'] = $customerDTO->getPaymentMethodNonce();
        }

        return $details;
    }
}
