<?php

namespace Coqmos\BrainTreeBundle\Services\Customer;

use Braintree\Customer;
use Braintree\CustomerSearch;
use Coqmos\BrainTreeBundle\DTOs\CustomerDTOInterface;
use Coqmos\BrainTreeBundle\Services\Customer\Abstraction\BaseCustomerService;

class SearchCustomerService extends BaseCustomerService
{
    /**
     * @param CustomerDTOInterface $customerDTO
     *
     * @return Customer
     */
    public function search(CustomerDTOInterface $customerDTO): Customer
    {
        $result = $this->customer
            ->search(
                $this->dtoToArray($customerDTO)
            )
        ;

        return $result->firstItem();
    }

    /**
     * @param CustomerDTOInterface $customerDTO
     *
     * @return array
     */
    private function dtoToArray(CustomerDTOInterface $customerDTO): array
    {
        $fields = [];

        if (null !== $customerDTO->getId()) {
            $fields[] = CustomerSearch::id()->is($customerDTO->getId());

            return $fields;
        }

        if (null !== $customerDTO->getEmail()) {
            $fields[] = CustomerSearch::email()->is($customerDTO->getEmail());
        }

        if (null !== $customerDTO->getFirstName()) {
            $fields[] = CustomerSearch::firstName()->is($customerDTO->getFirstName());
        }

        if (null !== $customerDTO->getLastName()) {
            $fields[] = CustomerSearch::lastName()->is($customerDTO->getLastName());
        }

        if (null !== $customerDTO->getPhone()) {
            $fields[] = CustomerSearch::phone()->is($customerDTO->getPhone());
        }

        if (null !== $customerDTO->getCompany()) {
            $fields[] = CustomerSearch::company()->is($customerDTO->getCompany());
        }

        if (null !== $customerDTO->getWebsite()) {
            $fields[] = CustomerSearch::website()->is($customerDTO->getWebsite());
        }

        return $fields;
    }
}
