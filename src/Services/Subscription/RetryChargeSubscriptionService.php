<?php

namespace Coqmos\BrainTreeBundle\Services\Subscription;

use Braintree\Result\Error;
use Braintree\Result\Successful;
use Coqmos\BrainTreeBundle\DTOs\SubscriptionDTOInterface;
use Coqmos\BrainTreeBundle\Services\Subscription\Abstraction\BaseSubscriptionService;
use Coqmos\BrainTreeBundle\Exception\BrainTreeException;

class RetryChargeSubscriptionService extends BaseSubscriptionService
{
    /**
     * @param SubscriptionDTOInterface $subscriptionDTO
     *
     * @return Successful
     *
     * @throws BrainTreeException
     */
    public function retry(SubscriptionDTOInterface $subscriptionDTO): Successful
    {
        $result = $this->subscription
            ->retryCharge(
                $subscriptionDTO->getSubscriptionId(),
                $subscriptionDTO->getAmount(),
                true
             )
        ;

        if ($result instanceof Error) {
            return $this->handleError($result);
        }

        return $result;
    }
}