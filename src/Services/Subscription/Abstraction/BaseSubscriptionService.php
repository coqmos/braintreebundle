<?php

namespace Coqmos\BrainTreeBundle\Services\Subscription\Abstraction;

use Braintree\SubscriptionGateway;
use Coqmos\BrainTreeBundle\Factory\BrainTreeFactory;
use Coqmos\BrainTreeBundle\Services\Abstraction\BrainTreeBaseService;

class BaseSubscriptionService extends BrainTreeBaseService
{
    /** @var SubscriptionGateway */
    protected $subscription;

    /**
     * {@inheritdoc}
     */
    public function __construct(BrainTreeFactory $brainTreeFactory)
    {
        parent::__construct($brainTreeFactory);
        $this->subscription = $this->getFactory()->subscription();
    }
}