<?php

namespace Coqmos\BrainTreeBundle\Services\Subscription;

use Braintree\Result\Error;
use Braintree\Subscription;
use Coqmos\BrainTreeBundle\DTOs\SubscriptionDTOInterface;
use Coqmos\BrainTreeBundle\Services\Subscription\Abstraction\BaseSubscriptionService;
use Coqmos\BrainTreeBundle\Exception\BrainTreeException;

class UpdateSubscriptionService extends BaseSubscriptionService
{
    /**
     * @param SubscriptionDTOInterface $subscriptionDTO
     *
     * @return Subscription
     *
     * @throws BrainTreeException
     */
    public function update(SubscriptionDTOInterface $subscriptionDTO): Subscription
    {
        $result = $this->subscription->update(
            $subscriptionDTO->getSubscriptionId(),
            $this->dtoToArray($subscriptionDTO)
        );

        if ($result instanceof Error) {
            return $this->handleError($result);
        }

        return $result->subscription;
    }


    /**
     * @param SubscriptionDTOInterface $subscriptionDTO
     *
     * @return array
     */
    private function dtoToArray(SubscriptionDTOInterface $subscriptionDTO): array
    {
        $details = [];

        if (null !== $subscriptionDTO->getPaymentMethodToken()) {
            $details['paymentMethodToken'] = $subscriptionDTO->getPaymentMethodToken();
        }

        if (null !== $subscriptionDTO->getAmount()) {
            $details['price'] = $subscriptionDTO->getAmount();
        }

        if (null !== $subscriptionDTO->getPlanId()) {
            $details['planId'] = $subscriptionDTO->getPlanId();
        }

        if (null !== $subscriptionDTO->getMerchantAccountId()) {
            $details['merchantAccountId'] = $subscriptionDTO->getMerchantAccountId();
        }

        return $details;
    }

}