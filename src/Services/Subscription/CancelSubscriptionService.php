<?php

namespace Coqmos\BrainTreeBundle\Services\Subscription;

use Braintree\Result\Error;
use Braintree\Subscription;
use Coqmos\BrainTreeBundle\DTOs\SubscriptionDTOInterface;
use Coqmos\BrainTreeBundle\Exception\BrainTreeException;
use Coqmos\BrainTreeBundle\Services\Subscription\Abstraction\BaseSubscriptionService;

class CancelSubscriptionService extends BaseSubscriptionService
{
    /**
     * @param SubscriptionDTOInterface $subscriptionDTO
     *
     * @return Subscription
     *
     * @throws BrainTreeException
     */
    public function cancel(SubscriptionDTOInterface $subscriptionDTO): Subscription
    {
        $result = $this->subscription->cancel($subscriptionDTO->getSubscriptionId());

        if ($result instanceof Error) {
            return $this->handleError($result);
        }

        return $result->subscription;
    }
}