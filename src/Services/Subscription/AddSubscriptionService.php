<?php

namespace Coqmos\BrainTreeBundle\Services\Subscription;

use Braintree\Result\Error;
use Braintree\Subscription;
use Coqmos\BrainTreeBundle\DTOs\SubscriptionDTOInterface;
use Coqmos\BrainTreeBundle\Exception\BrainTreeException;
use Coqmos\BrainTreeBundle\Services\Subscription\Abstraction\BaseSubscriptionService;

class AddSubscriptionService extends BaseSubscriptionService
{
    /**
     * @param SubscriptionDTOInterface $subscriptionDTO
     *
     * @return Subscription
     *
     * @throws BrainTreeException
     */
    public function create(SubscriptionDTOInterface $subscriptionDTO): Subscription
    {
        $result = $this
            ->subscription
            ->create($this->dtoToArray($subscriptionDTO)
        );

        if ($result instanceof Error) {
            return $this->handleError($result);
        }

        return $result->subscription;
    }

    /**
     * @param SubscriptionDTOInterface $subscriptionDTO
     *
     * @return array
     */
    private function dtoToArray(SubscriptionDTOInterface $subscriptionDTO): array
    {
        $details = [
            'paymentMethodToken'    => $subscriptionDTO->getPaymentMethodToken(),
            'planId'                => $subscriptionDTO->getPlanId()
        ];

        if (null !== $subscriptionDTO->getMerchantAccountId()) {
            $details['merchantAccountId'] =$subscriptionDTO->getMerchantAccountId();
        }

        return $details;
    }
}
