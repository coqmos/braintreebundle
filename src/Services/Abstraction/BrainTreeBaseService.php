<?php

namespace Coqmos\BrainTreeBundle\Services\Abstraction;

use Braintree\Gateway;
use Braintree\Result\Error;
use Coqmos\BrainTreeBundle\Exception\BrainTreeException;
use Coqmos\BrainTreeBundle\Factory\BrainTreeFactory;

class BrainTreeBaseService
{
    /**
     * @var BrainTreeFactory
     */
    protected $factory;

    /**
     * @param BrainTreeFactory $brainTreeFactory
     */
    public function __construct(BrainTreeFactory $brainTreeFactory)
    {
        $this->factory = $brainTreeFactory;
    }

    /**
     * @return Gateway
     *
     */
    protected function getFactory(): Gateway
    {
        return $this->factory->create();
    }

    /**
     * @param Error $error
     *
     * @throws BrainTreeException
     */
    protected function handleError(Error $error)
    {
        $errors = [];

        foreach ($error->errors->deepAll() as $errorItem) {
            /** @var Validation $errorItem */
            $errors[$errorItem->attribute] = $errorItem->message;
        }

        throw new BrainTreeException(json_encode($errors));
    }
}
