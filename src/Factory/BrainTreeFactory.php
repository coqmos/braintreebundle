<?php

namespace Coqmos\BrainTreeBundle\Factory;

use Braintree\Gateway;
use Braintree_Configuration;
use Braintree\Exception\Configuration;

class BrainTreeFactory
{
    /** @var string */
    private $environment;

    /** @var string */
    private $merchantId;

    /** @var string */
    private $publicKey;

    /** @var string */
    private $privateKey;

    /** @var Braintree_Configuration */
    private $config;

    /**
     * @param string $environment
     * @param string $merchantId
     * @param string $publicKey
     * @param string $privateKey
     *
     * @throws Configuration
     */
    public function __construct(string $environment, string $merchantId, string $publicKey, string $privateKey)
    {
        $this->environment = $environment;
        $this->merchantId  = $merchantId;
        $this->publicKey   = $publicKey;
        $this->privateKey  = $privateKey;

        $this->config      = new Braintree_Configuration(
            [
                'environment'   => $this->environment,
                'merchantId'    => $this->merchantId,
                'publicKey'     => $this->publicKey,
                'privateKey'    => $this->privateKey
            ]
        );
    }

    /**
     * @return Gateway
     */
    public function create(): Gateway
    {
        return new Gateway($this->config);
    }
}
