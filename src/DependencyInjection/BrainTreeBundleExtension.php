<?php

namespace Coqmos\BrainTreeBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;
use Exception;

class BrainTreeBundleExtension extends Extension
{
    /**
     * @param array $configs
     * @param ContainerBuilder $container
     *
     * @throws Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(  __DIR__ . '/../../config'));
        $loader->load('services.yaml');

        $this->loadServices($configs, $container);
    }

    /**
     * @param array $configs
     * @param ContainerBuilder $container
     */
    private function loadServices(array $configs, ContainerBuilder $container): void
    {
        $loader = new XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('brain_tree.xml');

        $config = $this->processConfiguration(new Configuration(), $configs);
        
        $brainTreeDef = clone $container->getDefinition('coqmos.factory.brain_tree_factory')
            ->setArgument('$environment', $config['environment'])
            ->setArgument('$merchantId', $config['merchantId'])
            ->setArgument('$publicKey', $config['publicKey'])
            ->setArgument('$privateKey', $config['privateKey'])
            ->setPublic(true)
        ;

        $container->setDefinition('coqmos.brain_tree', $brainTreeDef);
        $container->setAlias('Coqmos\BrainTreeBundle\Factory\BrainTreeFactory', 'coqmos.brain_tree');
    }
}