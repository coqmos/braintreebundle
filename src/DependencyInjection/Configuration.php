<?php

namespace Coqmos\BrainTreeBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder();
        $rootNode    = $treeBuilder->root('brain_tree_bundle');

        $rootNode
            ->fixXmlConfig('driver')
            ->children()
                ->scalarNode('environment')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('merchantId')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('publicKey')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('privateKey')->isRequired()->cannotBeEmpty()->end()
            ->end()
        ;

        return $treeBuilder;
    }
}