<?php

namespace Coqmos\BrainTreeBundle\DTOs;

interface SubscriptionDTOInterface
{
    /**
     * @return null|string
     */
    public function getSubscriptionId(): ?string ;

    /**
     * @return null|string
     */
    public function getPlanId(): ?string ;

    /**
     * @return null|string
     */
    public function getPaymentMethodToken(): ?string ;

    /**
     * @return float|null
     */
    public function getAmount(): ?float ;

    /**
     * @return null|string
     */
    public function getMerchantAccountId(): ?string ;
}
