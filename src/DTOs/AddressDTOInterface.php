<?php

namespace Coqmos\BrainTreeBundle\DTOs;

interface AddressDTOInterface
{
    /**
     * @return null|string
     */
    public function getCustomerId(): ?string;

    /**
     * @return null|string
     */
    public function getStreetAddress(): ?string;

    /**
     * @return null|string
     */
    public function getExtendedAddress(): ?string;

    /**
     * @return null|string
     */
    public function getLocality(): ?string;

    /**
     * @return null|string
     */
    public function getRegion(): ?string;

    /**
     * @return null|string
     */
    public function getPostalCode(): ?string;

    /**
     * @return null|string
     */
    public function getCountryCodeAlpha2(): ?string;
}
