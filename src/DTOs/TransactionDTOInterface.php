<?php

namespace Coqmos\BrainTreeBundle\DTOs;

interface TransactionDTOInterface
{
    /**
     * @return null|string
     */
    public function getTransactionId(): ?string;

    /**
     * @return float|null
     */
    public function getAmount(): ?float;


    /**
     * @return null|string
     */
    public function getPaymentMethodNonce(): ?string;

    /**
     * @return null|string
     */
    public function getOrderId(): ?string;

    /**
     * @return null|string
     */
    public function getDiscountAmount(): ?string;
}
