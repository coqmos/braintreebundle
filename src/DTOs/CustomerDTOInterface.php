<?php

namespace Coqmos\BrainTreeBundle\DTOs;

interface CustomerDTOInterface
{
    /**
     * @return null|string
     */
    public function getId(): ?string ;

    /**
     * @return null|string
     */
    public function getFirstName(): ?string;

    /**
     * @return null|string
     */
    public function getLastName(): ?string;

    /**
     * @return null|string
     */
    public function getCompany(): ?string;

    /**
     * @return null|string
     */
    public function getEmail(): ?string;

    /**
     * @return null|string
     */
    public function getPhone(): ?string;

    /**
     * @return null|string
     */
    public function getWebsite(): ?string;

    /**
     * @return null|string
     */
    public function getMerchantId(): ?string;

    /**
     * @return null|array
     */
    public function getAddresses(): ?array;

    /**
     * @return null|string
     */
    public function getPaymentMethodNonce(): ?string ;
}
