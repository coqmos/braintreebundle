<?php

namespace Coqmos\BrainTreeBundle;

use Coqmos\BrainTreeBundle\DependencyInjection\BrainTreeBundleExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class BrainTreeBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function getContainerExtension()
    {
        return new BrainTreeBundleExtension();
    }
}