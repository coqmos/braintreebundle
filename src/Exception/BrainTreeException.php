<?php

namespace Coqmos\BrainTreeBundle\Exception;

use Symfony\Component\HttpFoundation\Exception\RequestExceptionInterface;
use Symfony\Component\HttpFoundation\Response;
use Exception;

class BrainTreeException extends Exception implements RequestExceptionInterface
{
    /** @var int  */
    protected $code = Response::HTTP_BAD_REQUEST;

    protected $message = 'Braintree Exception';
}
