<?php
/**
 * Created by PhpStorm.
 * User: coqmo
 * Date: 9/8/2018
 * Time: 11:35 PM
 */
require_once ('../vendor/autoload.php');


use Coqmos\BrainTreeBundle\Services\Token;

$factory = new \Coqmos\BrainTreeBundle\Factory\BrainTreeFactory(
    'sandbox',
    '8j3j4nqnryxp4phs',
    'nxj75c397pdmnrs9',
    'c18ab28d9fc345a5bbb1e633df8bef80'

);


$token = new \Coqmos\BrainTreeBundle\Services\Widget(
    new Token($factory)
);

echo $token->create(10.50);